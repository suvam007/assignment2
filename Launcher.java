
//@ author Suvam Saha
//A program to demonstrate dynamic loading of classes(Loading of classes at runtime) and their introspection using *Reflection


//initiate a class using command line argument without making direct object 

import java.lang.reflect.Constructor;
import java.util.Scanner;
import java.lang.reflect.InvocationTargetException;
//import java.io.Serializable;


//create Customer class that implements Serializable interface
class Customer {
	//Enforce Encapsulation
	private String name;
	base b=new base();
	int id,age;
	
	//create constructor with 3 parameters
	public Customer(String name,int id,int age){
		this.name=name;
		this.age=age;
		this.id=id;
	}
	
	//create default constructor
	public Customer(){}
	//override toString method
	@Override
	public String toString(){
		return name+" "+id+" "+age;
	}
	public String name(){
		return "Name is "+name;
	}
}

//create class named base with 3 attributes
class base {
	private int a=20;
	protected int c=30;
	static int b=10;
	
	
}

//create class product
class Product{
	private String pname;
	private int pid,price;
	//create constructor with 3 parameters
	public Product(String pname,int pid,int price){
		this.pname=pname;
		this.price=price;
		this.pid=pid;
	}
		
	//create default constructor
	public Product(){}
	//override toString method
	@Override
	public String toString(){
		return pname+" "+pid+" "+price;
	}
	public String name(){
		return "Name is "+pname;
	}
}



//create CmdInstantiate class that hold initiate method that holds main part of the code
 class CmdInstantiate {
		//create method named initiate
    public void initiate(String[] args) {
        Scanner in = new Scanner(System.in);
		//save current class in cls
         Class cls = this.getClass();
		//create object array of the number of classes passed at runtime
        Object[] O = new Object[args.length];
			
		//for loop will continue for the number of class is passed as command line argument	
        for (int count=0;count<args.length;count++) {
            try {
				//the class will be dynamically loaded at the runtime
                cls = Class.forName(args[count]);
            } catch (ClassNotFoundException cnfe) {  //catch the exception if it the class doesn't exist
                System.out.println("Wrong Class Name");
            }
            try {
				//load constructors in constructor array
                Constructor[] cons = cls.getConstructors();
                System.out.println("Select Constructor: (Y for using, N for rejecting)");
                int i = 0;
				
                while (i < cons.length) {
                    //System.out.println("Select which Constructor you want to create instance of class with:");
                    System.out.println((i + 1) + ". " + cons[i]);
					//if block will execute if you type Y
                    if (in.next().equalsIgnoreCase("Y")) {
                        //load constructor
						Constructor con = cons[i];
						//load all the parameters in class array c
                        Class[] c = con.getParameterTypes();
                        i = 0;
                        System.out.println("Enter values for Parameter List: ");
						//create new object and load all parameters
                        Object[] arr = new Object[c.length];
                        while (i < c.length) {

                            System.out.println(c[i]);
                            String name = c[i].getName();
                            arr[i] = in.next();
                            if (name.equals("int")) {
                                arr[i] = Integer.parseInt(arr[i].toString());
                                c[i] = Integer.TYPE;
                            } else if (name.equals("java.lang.String")) {
                                c[i] = String.class;
                            }

                            i++;
                        }
                        //O = create(in, c, cls, arr, con);
                        O[count]=con.newInstance(arr);
                        System.out.println(O[count]);
                        break;
                    }
                    i++;
                }
            } catch (Exception cnfe) {
                System.out.println("" + cnfe.getMessage());
            }
        }
    }
}     

class Launcher{
	public static void main(String[] args) {
		CmdInstantiate cmd=new CmdInstantiate();
		if(args.length>0)
			cmd.initiate(args);
		else
			System.out.println("No Class Names found.");	
	}
}
